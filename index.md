---
layout: cv
title: Jakub Wysokiński's CV
---
# Jakub Wysokiński
Python developer

<div id="webaddress">
jakub.wysokinski@protonmail.com | 0048 737176370
</div>


## Experience
`2021.04 -`  
`Present`
__Nokia on behalf of SII - Python Software Engineer - Project: IQ DATA Generation__

Project description & team size:
Wrapping tool chains for one common interface. Team size: 2

Technologies:

Python, pydantic, artifactory, CI\CD, GitLab, GitLab Pipelines


`2021.01 -`  
`Present`
__Nokia on behalf of SII - Python Software Engineer - Project: TRX__

Project description & team size:
Creating tool which validates and process test data and real data from 4g\lte\5g antennas. Team size: 40

Beside developing tool I have implemented standarized formating (black) and changed release procedure so project does not need freezing branch.

Technologies:

Python, pandas, docker, artifactory, CI\CD, GitLab, GitLab Pipelines, Scrum

`2020.09 -`  
`2021.03`
__ZF Group / TRW Poland - Czestochowa Engineering Center - Python Software Engineer - Project: Bokken__

Project description & team size:

Creating tool which with test distribution tool, distributes and manages tests across many HILs, SILs etc. at once. Team size: 1

Technologies:

Python, ascyncio, docker, artifactory, CI\CD, GitLab, GitLab Pipelines

`2020.10 -`  
`2020.12`
__ZF Group / TRW Poland - Czestochowa Engineering Center - Python Software Engineer - Project: Project Managing tool__

Project description & team size:

Creating draft of a tool from which one can manage projects work, tests data etc. Team size: 4

Tasks:

Creating both backend and frontend (in draft format) to show capabilities.

Technologies:

Python, Django, NodeJs, GitLab, Kanban
        
`2017.10 -`  
`2020.12`
__ZF Group / TRW Poland - Czestochowa Engineering Center - Python Software Engineer - Project: Autosar Tester__

Project description & team size:

Creating tool which analyze, validates projects(example: car camera) achitecture after which it generates tests for that project. 
Team size was varying in time 7-4 teammembers. Due to change of size there was also change from SCRUM to Kanban.
This project was initially on SVN server, later on gerrit one and at the end on GitLab that is why there was used jenkins instead of gitlab pipelines.

Technologies: 

Python, Jinja2, nuitka, pandas, XML, SCRUM, Kanban, Jenkins, CI\CD, GitLab, Gerrit, Mercurial, BPMN


`2017.08 -`  
`2017.12`
__Częstochowa University of Technology - Scientific assistant__

After resigning from Master studies I was offered to continue my Bachelor thesis research as university's employee.

Technologies: 

Catia, Ansys WorkBench, Tecplot, Python, AutoCad  

`2017.07 -`  
`2017.08`
__ZF Group / TRW Poland  - Czestochowa Engineering Center - Intern as Python Software Engineer__

Internship in CEC Electronic department, after short period of internship company have offered me full time employment.

Technologies: Python, pandas, XML, SCRUM, SVN

`2014.07 -`
`2014.08`
__ZF Group / TRW Poland  - Seat Belts Plant -  Intern as Mechanical Engineer__

Internship in Development department - Modelling assemblies and theirs parts 

Technologies:

Catia, AutoCad, SolidWorks


## Programming related knowledge / experience

Python - 3.5 years  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Jinja2  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- pytest  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Nuitka  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- pandas  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- pydantic  

Object Oriented Programming  
Databases  
Continuous Improvement / Continuous Deployment  
Jenkins  
OpenShift  
Docker  
VCS - Git(Gerrit, GitLab, BitBucket) / Mercurial  
C / C++ / capl  
VBA  
MatLab  

## Other work related knowledge / experience

Agile(Scrum, Kanban, XP)     
JIRA  
Confluence  
AUTOSAR Standard (~3 year, mostly around application layer)  
BPMN  
Autosar Builder  
Catia  
Ansys WorkBench  
Tecplot  
MS Office  

## Education

`2017-`  
`2017`
__Częstochowa University of Technology, Power Engineering, Master degree__

Unfinished, resigned after starting work in ZF with Python.

`2013-`  
`2017`
__Częstochowa University of Technology, Mechanical Engineering, specialization Computer Modeling and Simulation, Bachelor degree__

Finished with Bachelor degree. Studies conducted in English

## Conferences

`2017.04`
__Seminary in Institute of Thermal Machinery, Częstochowa__  
*The only student in history of university who presented his work at this seminary*

`2017.03`
__International Conference of High Power Computer Users (KUKDM 2017), Zakopane__ 

## Certificates and courses
ISTQB  
Certified SolidWorks Associate
Course of using high power computers

<!-- ### Footer

Last updated: May 2013 -->


